const inputElement = document.querySelector('input');
const remainingCharsElement = document.getElementById('remaining-chars');
const maxAllowedChars = inputElement.maxLength;

inputElement.addEventListener('input', (event) => {
  const target = event.target;
  const remainingCounter = maxAllowedChars - target.value.length;

  remainingCharsElement.textContent = String(remainingCounter);

  if (remainingCounter <= 10) {
    target.classList.add('warning');
    remainingCharsElement.classList.add('warning');
  } else {
    target.classList.remove('warning');
    remainingCharsElement.classList.remove('warning');
  }
});
